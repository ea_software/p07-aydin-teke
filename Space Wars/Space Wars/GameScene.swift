//
//  GameScene.swift
//  Space Wars
//
//  Created by Emre AYDIN on 20.04.2017.
//  Copyright © 2017 Emre AYDIN. All rights reserved.
//

import SpriteKit
import GameplayKit

var score = 0

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    let score_label = SKLabelNode(fontNamed: "Cochin")
    let timer_label = SKLabelNode(fontNamed: "Cochin")
    
    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
    let colors = ["Orange", "Blue", "Green", "Purple", "Red", "White", "Yellow"]
    
    let bullet_category = 0x1 << 1
    let main_char_category = 0x1 << 2
    let enemy_category = 0x1 << 3
    let food_category = 0x1 << 4
    
    var is_right = false
    var is_up = false
    
    var food_c = 0
    
    var ship_id = 0
    
    var started: Bool = false
    var start_time: TimeInterval = 0
    var current_time: TimeInterval = 0
    var last_food_added: TimeInterval = 0
    var last_fire: TimeInterval = 0
    
    let main_char = SKSpriteNode(imageNamed: "rocket_new.png")
    let bg = SKSpriteNode(imageNamed: "background.jpg")
    
    let minimap = SKSpriteNode(imageNamed: "minimap.png")
    
    var locX: CGFloat = 0.0
    var locY: CGFloat = 0.0
    
    var multiLabel = SKMultilineLabel(text: "1.Ship-0 score: 0 2.Ship-1 score: 0 3.Ship-2 score: 0 ", labelWidth: 400, pos: CGPoint(x: 1792.0, y: 1420.8))
    
    
    struct Spaceship {
        var id = 0
        var size = 0
        var score = 0
        var color = ""
        var target = CGPoint(x: 0, y: 0)
        var is_alive = true
    }
    var spaceships = [Spaceship]()
    
    override func didMove(to view: SKView) {
        locX = self.size.width / 2
        locY = self.size.height / 2
        
        main_char.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        main_char.zRotation = CGFloat(Double.pi / 1.5)
        main_char.zPosition = 3
        main_char.xScale = 0.20
        main_char.yScale = 0.20
        main_char.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: main_char.size.width,
                                                                  height: main_char.size.height))
        main_char.name = "ship_0"
        main_char.physicsBody?.categoryBitMask = UInt32(main_char_category)
        main_char.physicsBody?.isDynamic = true
        main_char.physicsBody?.contactTestBitMask = UInt32(bullet_category)
        main_char.physicsBody?.collisionBitMask = 0
        main_char.physicsBody?.affectedByGravity = false
        
        
        let username_label = SKLabelNode(fontNamed: "Cochin")
        
        username_label.text = username
        username_label.fontSize = 40
        username_label.position = CGPoint(x: self.size.width / 2 , y: self.size.height / 2 - 150)
        username_label.color = .white
        username_label.zPosition = 4
        username_label.name  = "l_0"
        self.addChild(username_label)
        
        spaceships.append(Spaceship(id: ship_id, size: 1, score: 0, color: "Orange", target: CGPoint(x: 0, y: 0), is_alive: true))
        
        self.addChild(main_char)
        
        
        bg.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        bg.zPosition = 0
        bg.xScale = 2
        bg.yScale = 2
        
        self.addChild(bg)
        
        
        score_label.text = "Score: \(score)"
        score_label.fontSize = 75
        score_label.position = CGPoint(x: self.size.width / 2 * 0.2, y: self.size.height * 0.9)
        score_label.color = .white
        score_label.zPosition = 4
        score_label.name  = "non_clickable"
        self.addChild(score_label)
        
        timer_label.text = "Fight Starts In: "
        timer_label.fontSize = 50
        timer_label.position = CGPoint(x: self.size.width / 2 * 0.25, y: self.size.height * 0.1)
        timer_label.color = .white
        timer_label.zPosition = 4
        timer_label.name  = "non_clickable"
        self.addChild(timer_label)

        
        minimap.position = CGPoint(x: self.size.width * 0.88, y: self.size.height / 2 * 0.3)
        minimap.zPosition = 4
        minimap.xScale = 0.20
        minimap.yScale = 0.20
        self.addChild(minimap)
        
        
        let m = SKSpriteNode(imageNamed: "bullet_0.png")
        m.position = CGPoint(x: minimap.position.x, y: minimap.position.y)
        m.zPosition = 5
        m.name = "m_0"
        self.addChild(m)
        
        multiLabel.zPosition = 4
        self.addChild(multiLabel)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 2
        view.addGestureRecognizer(tap)
        
        self.physicsWorld.gravity = CGVector(dx:0, dy: 0)
        self.physicsWorld.contactDelegate = self
        
        for _ in 0...50{
            self.add_food()
        }
        
        
        add_ship()
        add_ship()
        
        add_ship()
        add_ship()
        
        add_ship()
        add_ship()
        
        update_leadership()
        
    }
    
    func doubleTapped() {
        self.add_bullet(pos: main_char.position, target: CGPoint(x:locX, y:locY), id: 0)
    }
    
    func add_ship(){
        ship_id += 1
        let ship = SKSpriteNode(imageNamed: "rocket_new.png")
        let random_x = Int(arc4random() % UInt32(self.size.width * 2))  - Int(self.size.width)
        let random_y = Int(arc4random() % UInt32(self.size.height * 2))  - Int(self.size.height)
        ship.position = CGPoint(x: random_x, y: random_y)
        ship.zRotation = CGFloat(Double.pi / 1.5)
        ship.zPosition = 3
        ship.xScale = 0.20
        ship.yScale = 0.20
        ship.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: main_char.size.width,
                                                             height: main_char.size.height))
        ship.name = "ship_\(ship_id)"
        ship.physicsBody?.categoryBitMask = UInt32(enemy_category)
        ship.physicsBody?.isDynamic = true
        ship.physicsBody?.contactTestBitMask = UInt32(bullet_category)
        ship.physicsBody?.collisionBitMask = 0
        ship.physicsBody?.affectedByGravity = false
        
        spaceships.append(Spaceship(id: ship_id, size: 1, score: 0, color: colors[ship_id], target: CGPoint(x: 0, y: 0), is_alive: true))
        self.addChild(ship)
        
        
        
        let m = SKSpriteNode(imageNamed: "bullet_\(ship_id).png")
        let x_distance = main_char.position.x - CGFloat(random_x)
        let y_distance = main_char.position.y - CGFloat(random_y)
        m.position = CGPoint(x: minimap.position.x - x_distance / 50 , y: minimap.position.y - y_distance / 50)
        m.zPosition = 5
        m.name = "m_\(ship_id)"
        self.addChild(m)
        
        
        let username_label = SKLabelNode(fontNamed: "Cochin")
        username_label.text = colors[ship_id]
        username_label.fontSize = 40
        username_label.position = CGPoint(x: CGFloat(random_x) , y: CGFloat(random_y) - 150)
        username_label.color = .white
        username_label.zPosition = 4
        username_label.name  =  "l_\(ship_id)"
        self.addChild(username_label)
        

    }
    
    func update_leadership(){
        var text = ""
        var sorted_spaceships : [Spaceship] = spaceships
        sorted_spaceships = spaceships.sorted(by: { (s1: Spaceship, s2: Spaceship) -> Bool in
            return s1.score > s2.score
        })
        var c = 1
        for (index, element) in sorted_spaceships.enumerated() {
            if element.is_alive{
                text += "\(c).\(element.color) score: \(element.score)     "
                c+=1
            }
        }
        multiLabel.text = text
    }
    
    func find_distance(a: CGPoint, b: CGPoint) -> CGFloat {
        let xDist = a.x - b.x
        let yDist = a.y - b.y
        return CGFloat(sqrt((xDist * xDist) + (yDist * yDist)))
    }
    
    func move_minimap_items(){
        for i in 1...spaceships.count{
            self.enumerateChildNodes(withName: "m_\(i)", using: {(node, stop) -> Void in
                if let m = node as? SKSpriteNode{
                    var x_pos: CGFloat = 0.0
                    var y_pos: CGFloat = 0.0
                    
                    
                    self.enumerateChildNodes(withName: "ship_\(i)", using: {(node, stop) -> Void in
                        if let ship = node as? SKSpriteNode{
                            x_pos = ship.position.x
                            y_pos = ship.position.y
                        }
                    })
                    
                    let x_distance = self.main_char.position.x - CGFloat(x_pos)
                    let y_distance = self.main_char.position.y - CGFloat(y_pos)
                    
                    m.position = CGPoint(x: self.minimap.position.x - x_distance / 50 , y: self.minimap.position.y - y_distance / 50)
                }
                
            })
        }
    }
    
    func move_ships(){
        var counter = 1
        for i in 1...spaceships.count{
        self.enumerateChildNodes(withName: "ship_\(i)", using: {(node, stop) -> Void in
            if let ship = node as? SKSpriteNode{
                var angle: CGFloat = 0
                var distance: CGFloat = 999999.9
                
                //first 30 seconds don't shot each other
                if self.current_time - self.start_time > 30{
                    
                    let distance_main = abs(self.find_distance(a: ship.position, b: self.main_char.position))
                    
                    if distance_main < distance && self.spaceships[counter].score > score{
                        distance = distance_main
                        angle = self.find_angle(p1: ship.position, p2: self.main_char.position, loc: ship.position)
//                        NSLog("\(distance_main) vurucam")
                        self.spaceships[counter].target = self.main_char.position
                        
                    }
                    var ship_c = 0
                    for k in 1...self.spaceships.count{
                        self.enumerateChildNodes(withName: "ship_\(k)", using: {(node, stop) -> Void in
                            if let target_ship = node as? SKSpriteNode{
                                let distance_temp = abs(self.find_distance(a: ship.position, b: target_ship.position))
                                if distance_temp < distance && self.spaceships[counter].score > self.spaceships[ship_c].score {
                                    distance = distance_temp
                                    angle = self.find_angle(p1: ship.position, p2: target_ship.position, loc: ship.position)
                                    self.spaceships[counter].target = target_ship.position
                                }
                                ship_c += 1
                            }
                        })
                    }
                }
                
                self.enumerateChildNodes(withName: "food", using: {(node, stop) -> Void in
                    if let food = node as? SKSpriteNode{
                        let distance_temp = abs(self.find_distance(a: ship.position, b: food.position))
                        if distance_temp < distance || angle == 0{
                            distance = distance_temp
                            angle = self.find_angle(p1: ship.position, p2: food.position, loc: ship.position)
                            self.spaceships[counter].target = food.position
                        }
                    }
                })
                
                
                
                let float_angle = self.find_angle(p1: ship.position, p2:  self.spaceships[counter].target, loc: ship.position)
                
                let widthSpeed = abs(cos(float_angle)) / 1.5
                let heightSpeed = abs(sin(float_angle)) / 1.5
                
                let x_speed = CGFloat(cos(angle)) + widthSpeed
                let y_speed = CGFloat(sin(angle)) + heightSpeed
                
                let move_action = SKAction.moveBy(x: x_speed * 4, y: y_speed * 4, duration: 0)
               
                let rotate_angle = angle - ship.zRotation
                let rotate = SKAction.rotate(byAngle: rotate_angle, duration: 0)
                
                ship.run(rotate)
                ship.run(move_action)
                
                
                
                counter += 1
            }
        })
        }
    }
    func set_label_position(){
        for i in 1...spaceships.count{
            let label = self.childNode(withName: "l_\(i)")
            let ship = self.childNode(withName: "ship_\(i)")
            label?.position = CGPoint(x: (ship?.position.x)!, y: (ship?.position.y)! - 150)
        }
        
    }
    func delete_death(){
        for i in 0...self.spaceships.count{
            self.enumerateChildNodes(withName: "ship_\(i)", using: {(node, stop) -> Void in
                if let ship = node as? SKSpriteNode{
                    if self.spaceships[i].score < 0{
                        if i == 0{
                            let game_scene = GameOverScene(size: self.size)
                            game_scene.scaleMode = self.scaleMode
                            
                            let scene_transition = SKTransition.crossFade(withDuration: 0.2)
                            self.view!.presentScene(game_scene, transition: scene_transition)
                            
                        }
                        let fade = SKAction.fadeAlpha(to: 0, duration: 2.0)

                        ship.run(fade)
                        ship.removeFromParent()
                        
                        self.spaceships[i].is_alive = false
                        
                        let label = self.childNode(withName: "l_\(i)")
                        label?.removeFromParent()
                        
                        let m = self.childNode(withName: "m_\(i)")
                        m?.removeFromParent()

                        
                    }
                }
            })
        }
    }
    
    func fire_ships(){
        var counter = 1
        
        for i in 1...self.spaceships.count{
            self.enumerateChildNodes(withName: "ship_\(i)", using: {(node, stop) -> Void in
            if let ship = node as? SKSpriteNode{
                let target_pos = self.spaceships[counter].target
                self.add_bullet(pos: ship.position, target: target_pos, id: counter)
                counter += 1
                }
            })
        }
    }
    
    func add_food(){
        let random_index = Int(arc4random() % 10)
        let food = SKSpriteNode(imageNamed: "d_\(random_index+1).png")
        
        
        let random_x = Int(arc4random() % UInt32(self.size.width * 3))  - Int(self.size.width)
        let random_y = Int(arc4random() % UInt32(self.size.height * 3))  - Int(self.size.height)
        food.position = CGPoint(x: random_x, y: random_y)
        food.xScale = 0.9
        food.yScale = 1
        food.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: food.size.width * 1.5,
                                                             height: food.size.height * 1.5))
        food.physicsBody?.categoryBitMask = UInt32(food_category)
        food.physicsBody?.isDynamic = true
        food.physicsBody?.contactTestBitMask = UInt32(bullet_category)
        food.physicsBody?.collisionBitMask = 0
        food.physicsBody?.affectedByGravity = false
        food.physicsBody?.usesPreciseCollisionDetection = true
        food.name = "food"
        self.addChild(food)
    }
    
    func add_bullet(pos: CGPoint, target: CGPoint, id: Int){
//        let size = spaceships[id].size / 5
//        for i in stride(from: 0, to: size+1, by: 1){
        
            let angle = find_angle(p1: pos, p2: target, loc: pos)
            
            let bullet = SKSpriteNode(imageNamed: "bullet_\(id).png")
        
                bullet.position = CGPoint(x: pos.x, y: pos.y)
           
            bullet.xScale = 1
            bullet.yScale = 1
            bullet.zPosition = 2
            bullet.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: bullet.size.width,
                                                                   height: bullet.size.height))
            
            bullet.physicsBody?.categoryBitMask = UInt32(bullet_category)
            bullet.physicsBody?.isDynamic = true
            bullet.physicsBody?.contactTestBitMask = UInt32(food_category) | UInt32(main_char_category) | UInt32(enemy_category)
            bullet.physicsBody?.collisionBitMask = 0
            bullet.physicsBody?.affectedByGravity = false
            bullet.physicsBody?.usesPreciseCollisionDetection = true
            bullet.name = "bullet_\(id)"
            let x_speed = CGFloat(cos(angle) * 1000)
            let y_speed = CGFloat(sin(angle) * 1000)
            
            //        let vector = CGVector(dx: sin(angle) * 3000, dy: cos(angle) * 3000)
            //        let vector = CGVector(dx: cos(angle), dy: sin(angle))
            
            //        NSLog("\(angle)/")
            
            let move_action = SKAction.moveBy(x: x_speed, y: y_speed, duration: 2)
            let fade = SKAction.fadeOut(withDuration: 0)
            
            let sequence = SKAction.sequence([move_action, fade])
            bullet.run(sequence, withKey: "moving")
        
            self.enumerateChildNodes(withName: "ship_\(id)", using: {(node, stop) -> Void in
                if let ship = node as? SKSpriteNode{
                    let rotate_angle = angle - ship.zRotation
                    let rotate = SKAction.rotate(byAngle: rotate_angle, duration: 0)
                    ship.run(rotate)
                }
            })

            self.addChild(bullet)
//        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches{
            //let node = atPoint(touch.location(in: self))
            
            locX = touch.location(in: self).x
            locY = touch.location(in: self).y
            
            //            let angle = atan2(locY - main_char.position.y, locX - main_char.position.x)
            
            //            let rotation = SKAction.rotate(byAngle:  angle - CGFloat(Double.pi / 2), duration: 0.1)
            //            main_char.run(rotation)
            //
            //rotateByAngle: angle/4.0 duration:0
            let angle = find_angle(p1: main_char.position, p2: CGPoint(x:locX, y:locY), loc: main_char.position) - main_char.zRotation
            let animate = SKAction.rotate(byAngle: angle, duration: 0)
            main_char.run(animate)
            
            //            NSLog("Rotation: \(main_char.zRotation)")
            
            //            NSLog("Angle: \(angle)")
            
        }
    }
    
    func deg2rad(d: CGFloat) -> CGFloat{
        return CGFloat(d) * CGFloat(Double.pi) / 180.0
    }
    
    func find_angle(p1: CGPoint, p2: CGPoint, loc:CGPoint) -> CGFloat{
        let v1 = CGVector(dx: p1.x - loc.x, dy: p1.y - loc.y)
        let v2 = CGVector(dx: p2.x - loc.x, dy: p2.y - loc.y)
        
        
        let angle = atan2(v2.dy, v2.dx) - atan2(v1.dy, v1.dx)
        
        
        var deg = angle * CGFloat(180.0 / .pi)
        
        if deg < 0 { deg += 360.0 }
        
        return CGFloat(deg) * CGFloat(Double.pi) / 180.0
    }
    
    func find_bullet_angle(p1: CGPoint, p2: CGPoint, loc:CGPoint) -> CGFloat{
        let v1 = CGVector(dx: p1.x - loc.x, dy: p1.y - loc.y)
        let v2 = CGVector(dx: p2.x - loc.x, dy: p2.y - loc.y)
        
        
        let angle = atan2(v2.dy, v2.dx) - atan2(v1.dy, v1.dx)
        
        
        var deg = angle * CGFloat(180.0 / .pi)
        
        if deg < 0 { deg += 360.0 }
        
        if deg >= 90 && deg <= 180{
            deg -= 90
        }else if (deg >= 180 && deg <= 270){
            deg -= 180
            
        }else if (deg >= 270 && deg <= 360){
            deg -= 270
            
        }
        return CGFloat(deg)
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        var first_body = SKPhysicsBody()
        var second_body = SKPhysicsBody()
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask{
            first_body = contact.bodyA
            second_body = contact.bodyB
        }else{
            first_body = contact.bodyB
            second_body = contact.bodyA
        }
        
        if (first_body.categoryBitMask & UInt32(bullet_category) != 0 &&
            second_body.categoryBitMask & UInt32(food_category) != 0){
            second_body.node?.removeFromParent()
            let bullet_id = first_body.node?.name?.replacingOccurrences(of: "bullet_", with: "")
            
            first_body.node?.removeFromParent()
            
            
            if bullet_id != nil{
                if bullet_id == "0"{
                    score += 1
                    score_label.text = "Score: \(score)"
                    
                    let saved = UserDefaults()
                    var high_score = saved.integer(forKey: "high_score")
                    
                    if score > high_score{
                        high_score = score
                        saved.set(high_score, forKey: "high_score")
                    }
                    
                }
                spaceships[Int(bullet_id!)!].size += 1
                spaceships[Int(bullet_id!)!].score += 1
            
                increase_scales(ship_id: Int(bullet_id!)!)
            
                food_c -= 1
            }
            
        }
        
        else if (first_body.categoryBitMask & UInt32(bullet_category) != 0 &&
            second_body.categoryBitMask & UInt32(main_char_category) != 0){
            
            if self.current_time - self.start_time > 30{
            let bullet_id = first_body.node?.name?.replacingOccurrences(of: "bullet_", with: "")
            if bullet_id != "0"{
                score -= 1
                score_label.text = "Score: \(score)"
                
                spaceships[0].score -= 1
                spaceships[0].size -= 1
                
                decrease_scale(ship_id: 0)
                
                if bullet_id != nil{
                    spaceships[Int(bullet_id!)!].size += 1
                    spaceships[Int(bullet_id!)!].score += 1
                    increase_scales(ship_id: Int(bullet_id!)!)
                }
                
                first_body.node?.removeFromParent()
            }
            }
        }
        else if (first_body.categoryBitMask & UInt32(bullet_category) != 0 &&
            second_body.categoryBitMask & UInt32(enemy_category) != 0){
            
            if self.current_time - self.start_time > 30{
            let bullet_id = first_body.node?.name?.replacingOccurrences(of: "bullet_", with: "")
            let enemy_id = second_body.node?.name?.replacingOccurrences(of: "ship_", with: "")
            
            if bullet_id != enemy_id{
                if bullet_id != nil && enemy_id != nil{
                    if bullet_id == "0"{
                        score += 1
                        score_label.text = "Score: \(score)"
                        
                        let saved = UserDefaults()
                        var high_score = saved.integer(forKey: "high_score")
                        
                        if score > high_score{
                            high_score = score
                            saved.set(high_score, forKey: "high_score")
                        }
                    }
                    spaceships[Int(enemy_id!)!].score -= 1
                    spaceships[Int(enemy_id!)!].size -= 1
                    
                    spaceships[Int(bullet_id!)!].score += 1
                    spaceships[Int(bullet_id!)!].size += 1
                    
                    decrease_scale(ship_id: Int(enemy_id!)!)
                    increase_scales(ship_id: Int(bullet_id!)!)
                    
                }
            
                first_body.node?.removeFromParent()
            }
            }
        }
    }
    func decrease_scale(ship_id: Int){
        var counter = 1
        if ship_id == 0{
            let x_scale = main_char.xScale - 0.01
            let scale_action = SKAction.scale(to: x_scale, duration: 0)
            main_char.run(scale_action)
            
        }
        else{
            for i in 1...self.spaceships.count{
                self.enumerateChildNodes(withName: "ship_\(i)", using: {(node, stop) -> Void in
                if let ship = node as? SKSpriteNode{
                    if ship_id == counter{
                        let x_scale = ship.xScale - 0.01
                        let scale_action = SKAction.scale(to: x_scale, duration: 0)
                        ship.run(scale_action)
                    }
                    counter += 1
                }
            })
            }
        }
    }
    
    func increase_scales(ship_id: Int){
        var counter = 1
        if ship_id == 0{
            let x_scale = main_char.xScale + 0.01
            let scale_action = SKAction.scale(to: x_scale, duration: 0)
            main_char.run(scale_action)
            
        }
        else{
            for i in 1...self.spaceships.count{
                self.enumerateChildNodes(withName: "ship_\(i)", using: {(node, stop) -> Void in
                if let ship = node as? SKSpriteNode{
                    if ship_id == counter{
                        let x_scale = ship.xScale + 0.01
                        let scale_action = SKAction.scale(to: x_scale, duration: 0)
                        ship.run(scale_action)
                    }
                    counter += 1
                }
            })
            }
        }
    }
    
    func clean_bullets(){
        for i in 0...spaceships.count{
            self.enumerateChildNodes(withName: "bullet_\(i)", using: {(node, stop) -> Void in
                if let bullet = node as? SKSpriteNode{
                    if !bullet.hasActions(){
                        bullet.removeFromParent()
                    }
                }
            })
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        //        let widthSpeed = 1 / 1024 * abs((locX - self.size.width / 2))
        //        let heightSpeed = 1 / 768 * abs((locY - self.size.height / 2))
        //
        current_time = currentTime
        if self.current_time - self.start_time < 30{
            let text = String(format: "Fight Starts In: %.2f", 30.0 - self.current_time + self.start_time)
            timer_label.text = text
        }else{
            timer_label.text = "Fight started."
            delete_death()

        }
        if !started{
            start_time = currentTime
            started = true
        }
        
        move_ships()
        move_minimap_items()
        set_label_position()
        clean_bullets()
        update_leadership()
        
        let angle = find_angle(p1: main_char.position, p2: CGPoint(x:locX, y:locY), loc: main_char.position)
        let widthSpeed = abs(cos(angle)) / 1.5
        let heightSpeed = abs(sin(angle)) / 1.5
        
        
        if locX - self.size.width / 2 > 0 { //right
            
            is_right = true
            bg.position = CGPoint(x: bg.position.x - widthSpeed, y: bg.position.y)
            
            self.enumerateChildNodes(withName: "food", using: {(node, stop) -> Void in
                if let food = node as? SKSpriteNode{
                    food.position = CGPoint(x:  food.position.x - widthSpeed * 10, y: food.position.y)
                }
            })
            for i in 1...self.spaceships.count{
                self.enumerateChildNodes(withName: "ship_\(i)", using: {(node, stop) -> Void in
                if let ship = node as? SKSpriteNode{
                    ship.position = CGPoint(x:  ship.position.x - widthSpeed * 10, y: ship.position.y)
                }
                    
            })
            }
            
            for i in 0...spaceships.count{
                self.enumerateChildNodes(withName: "bullet_\(i)", using: {(node, stop) -> Void in
                    if let bullet = node as? SKSpriteNode{
                        bullet.position = CGPoint(x:  bullet.position.x - widthSpeed * 10, y: bullet.position.y)
                    }
                })
            }
            
            
            
        } else if locX - self.size.width / 2 < 0 { //left
            
            is_right = false
            bg.position = CGPoint(x: bg.position.x + widthSpeed, y: bg.position.y)
            self.enumerateChildNodes(withName: "food", using: {(node, stop) -> Void in
                if let food = node as? SKSpriteNode{
                    food.position = CGPoint(x:  food.position.x + widthSpeed * 10, y: food.position.y)
                }
            })
            for i in 1...self.spaceships.count{
                self.enumerateChildNodes(withName: "ship_\(i)", using: {(node, stop) -> Void in
                if let ship = node as? SKSpriteNode{
                    ship.position = CGPoint(x:  ship.position.x + widthSpeed * 10, y: ship.position.y)
                }
            })
            }
            for i in 0...spaceships.count{
                self.enumerateChildNodes(withName: "bullet_\(i)", using: {(node, stop) -> Void in
                    if let bullet = node as? SKSpriteNode{
                        bullet.position = CGPoint(x:  bullet.position.x + widthSpeed * 10, y: bullet.position.y)
                    }
                })
            }
            
        }
        
        if locY - self.size.height / 2 > 0 { //up
            is_up = true
            bg.position = CGPoint(x: bg.position.x, y: bg.position.y - heightSpeed)
            self.enumerateChildNodes(withName: "food", using: {(node, stop) -> Void in
                if let food = node as? SKSpriteNode{
                    food.position = CGPoint(x:  food.position.x, y: food.position.y - heightSpeed * 10)
                }
            })
            for i in 1...self.spaceships.count{
                self.enumerateChildNodes(withName: "ship_\(i)", using: {(node, stop) -> Void in
                if let ship = node as? SKSpriteNode{
                    ship.position = CGPoint(x:  ship.position.x, y: ship.position.y - heightSpeed * 10)
                }
            })
            }
            
            for i in 0...spaceships.count{
                self.enumerateChildNodes(withName: "bullet_\(i)", using: {(node, stop) -> Void in
                    if let bullet = node as? SKSpriteNode{
                        bullet.position = CGPoint(x:  bullet.position.x, y: bullet.position.y  - heightSpeed * 10)
                    }
                })
            }
            
        } else if locY - self.size.height / 2 < 0 { //down
            
            is_up = false
            bg.position = CGPoint(x: bg.position.x, y: bg.position.y + heightSpeed)
            self.enumerateChildNodes(withName: "food", using: {(node, stop) -> Void in
                if let food = node as? SKSpriteNode{
                    food.position = CGPoint(x:  food.position.x, y: food.position.y + heightSpeed * 10)
                }
            })
            
            for i in 1...self.spaceships.count{
                self.enumerateChildNodes(withName: "ship_\(i)", using: {(node, stop) -> Void in
                if let ship = node as? SKSpriteNode{
                    ship.position = CGPoint(x:  ship.position.x, y: ship.position.y + heightSpeed * 10)
                }
            })
            }
            for i in 0...spaceships.count{
                self.enumerateChildNodes(withName: "bullet_\(i)", using: {(node, stop) -> Void in
                    if let bullet = node as? SKSpriteNode{
                        bullet.position = CGPoint(x:  bullet.position.x, y: bullet.position.y  + heightSpeed * 10)
                    }
                })
            }
        }
        if currentTime - self.last_food_added > 0.2 && food_c < 500 {
            self.last_food_added = currentTime + 0.2
            self.add_food()
            food_c += 1
        }
        if currentTime - self.last_fire > 0.3 {
            self.last_fire = currentTime + 0.3
            self.fire_ships()
        }
    }
    
}
