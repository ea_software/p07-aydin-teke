//
//  Spaceship.swift
//  Space Wars
//
//  Created by Emre AYDIN on 28.04.2017.
//  Copyright © 2017 Emre AYDIN. All rights reserved.
//

import UIKit

class Spaceship {
    
    var size = Int()
    var color = Int()
    
    init (_ size: Int, _ color: Int) {
        self.size = size
        self.color = color
    }
    
}
